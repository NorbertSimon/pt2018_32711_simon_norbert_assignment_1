package edu.ro.utcn.calc.pt_polynomial_calc_Model;

import java.util.*;

public class Polinom {
	private List<Monom> terms;

	public List<Monom> getTerms() {
		return terms;
	}

	public void setTerms(List<Monom> terms) {
		this.terms = terms;
	}

	public Polinom() {
		terms= new ArrayList<Monom>();
	}
	
	public void addMonom(Monom m) {
		boolean found = false;
		for(Monom i:terms) {
			if(i.getGrad()==m.getGrad())
			{
				i.addMonom(m);
				found=true;
			}
		}
		if(!found) {
			terms.add(m);
		}
	}
	
	@Override
	public String toString() {
		
		String reprez="";
		for(Monom m:terms) {
			reprez+=m.toString();
		}
		return reprez;
		
	}

}
