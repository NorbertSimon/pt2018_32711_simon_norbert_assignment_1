package edu.ro.utcn.calc.pt_polynomial_calc_Model;

public class Monom {
	private int grad;
	private double coeficient;

	public Monom(double coeficient, int grad) {
		super();
		this.setGrad(grad);
		this.setCoeficient(coeficient);
	}

	public int getGrad() {
		return grad;
	}

	public void setGrad(int grad) {
		if (grad < 0) {
			return;
		}
		this.grad = grad;
	}

	public double getCoeficient() {
		return coeficient;
	}

	public void setCoeficient(double coeficient) {
		this.coeficient = coeficient;
	}

	public void addMonom(Monom n) {
		if (this.grad == n.grad) {
			this.coeficient += n.coeficient;
		}
	}
	
	@Override
	public String toString() {
		if(coeficient<0) {
			return "-"+coeficient + "x^" + grad;
		}
		else
			return "+"+coeficient + "x^" + grad;
	}

	}
