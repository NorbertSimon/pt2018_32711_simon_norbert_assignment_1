package edu.ro.utcn.calc.pt_polynomial_calc_Model;

import java.util.List;

public class Operation {
	public static Polinom add(Polinom p1, Polinom p2) {
		List<Monom> terms2= p2.getTerms();
		for(Monom m : terms2) {
			p1.addMonom(m);
		}
		return p1;
	}
	public static Polinom subtract(Polinom p1, Polinom p2) {
		List<Monom> terms2= p2.getTerms();
		//Need to implement
		for(Monom m : terms2) {
			p1.addMonom(m);
		}
		return p1;
	}

}
