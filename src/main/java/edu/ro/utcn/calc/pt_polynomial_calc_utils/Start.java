package edu.ro.utcn.calc.pt_polynomial_calc_utils;

import edu.ro.utcn.calc.pt_polynomial_calc_Model.Monom;
import edu.ro.utcn.calc.pt_polynomial_calc_Model.Operation;
import edu.ro.utcn.calc.pt_polynomial_calc_Model.Polinom;

public class Start {

	public static void main(String[] args) {
		Monom m1 = new Monom(3,4);
		Monom m2 = new Monom(2,4);
		Monom m3 = new Monom(5,3);
		Monom m4 = new Monom(2,2);
		
		Polinom pn1 = new Polinom();
		Polinom pn2 = new Polinom();
		
		pn1.addMonom(new Monom(3,4));
		pn1.addMonom(m2);
		pn1.addMonom(m3);
		pn1.addMonom(m4);
		
		pn2.addMonom(new Monom(3,4));
		pn2.addMonom(m2);
		pn2.addMonom(m3);
		pn2.addMonom(m4);
		
		
		
		System.out.println(pn1);
		System.out.println(pn2);
		Polinom pn = Operation.add(pn1, pn2);
		
		System.out.println(pn);
		

	}

}
